const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

	//retrieve all Active products
	router.get('/activeProducts',productControllers.getActiveProducts);
	//retieve single product <productId>
	router.get('/getSingleProduct/:orderId',productControllers.getSingleProduct);
	//add product (admin)
	router.post('/addProduct',verify,verifyAdmin,productControllers.addProduct);
	//update poduct information (admin) <productId>
	router.put("/updateProduct/:orderId",verify,verifyAdmin,productControllers.updateProduct);
	//archive product (admin) <productId>
	router.delete("/archiveProduct/:orderId",verify,verifyAdmin,productControllers.archiveProduct);
	//retrieve all orders from all users(admin)
	router.get('/getAllOrders',verify,verifyAdmin,productControllers.getAllOrders);

	//STRECHGOAL activate product (admin) <productId>
	router.put("/activateProduct/:orderId",verify,verifyAdmin,productControllers.activateProduct);
	//STRECHGOAL displayPerOrder
	router.get('/displayPerOrder/:orderId',verify,productControllers.displayPerOrder);



module.exports = router;