const express = require("express");
const router = express.Router();

const userControllers = require ("../controllers/userControllers");
// console.log(userControllers);

const auth = require("../auth");
//destructure
const {verify,verifyAdmin} = auth;

//user registration <insert info in req.body>
router.post("/",userControllers.registerUser);
//user authentication <email and password in req.body>
router.post("/login",userControllers.loginUser);
//Logged in regular user checkout(Create Order)
router.post("/createOrder",verify,userControllers.createOrder);//fix me
//get user details
router.get("/details",verify,userControllers.getUserDetails);
//set user as admin(admin) <userId>
router.put("/makeAdmin/:userId",verify,verifyAdmin,userControllers.makeAdmin);
//retrieve authenticated users orders
router.get("/getOrders",verify,userControllers.getOrders);

//get all orders (admin only)
router.get('/orders',verify,verifyAdmin,userControllers.getAllOrders);

module.exports = router;







