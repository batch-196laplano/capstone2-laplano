const express = require ("express");
const mongoose = require ("mongoose");
const app = express();
const port = 4000;

// const bcrypt = require("bcrypt");
// const User = require("./models/User");

mongoose.connect("mongodb+srv://admin:admin123@cluster0.qosmb.mongodb.net/ecommerceAPI?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error."))
db.once('open',()=>console.log("Connected to MongoDB"));

app.use(express.json());

const productRoutes = require('./routes/productRoutes');
// console.log(courseRoutes);
app.use('/products',productRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

// app.post('/users',(req,res)=>{
// 	const hashedPw = bcrypt.hashSync(req.body.password,10);
// 	// console.log(hashedPw);

// 	let newUser = new User({
// 		firstname: req.body.firstname,
// 		lastname: req.body.lastname,
// 		email: req.body.email,
// 		password: hashedPw,
// 		mobileNo: req.body.mobileNo
// 	})
// 		newUser.save()
// 		.then(result => res.send(result))
// 		.catch(error => res.send(error))
// })

app.listen(port,()=> console.log(`Express API running at port 4000`));