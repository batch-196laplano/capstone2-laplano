const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//user registration 
module.exports.registerUser = (req,res)=>{
	const hashedPw = bcrypt.hashSync(req.body.password,10);
	let newUser = new User({
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	})
		newUser.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
}

//user authentication
module.exports.loginUser = (req,res) => {
	//email and password in req.body
	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser =>{
		if(foundUser===null){
			return res.send({message: "No User Found."})

		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			if(isPasswordCorrect){
			//create a token for the user if the password is correct
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			}else{
				return res.send({message: "Incorrect Password"});
			}
		}
	})
}

//Logged in regular user checkout(Create Order)
module.exports.createOrder = async (req,res) => {
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}
	//step1 update user
	let isUserUpdated = await User.findById(req.user.id).then(user=> {
		// console.log(user)
		let newOrder = {
			totalAmount: req.body.totalAmount, //quantity*price manual entry as of now
			products:[
			{
				productId: req.body.productId,
				quantity: req.body.quantity
			}
			]
		}
		user.orders.push(newOrder)
		return user.save().then(user=>true).catch(err=>err.message)
	})
	// console.log(isUserUpdated);
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}
	
	//step2 update product
	let isOrderUpdated = await Product.findById(req.body.productId).then(product=> {
		//console.log(product); //contains the found product
		let orders = {
			orderId: req.body.productId,
			quantity: req.body.quantity,
			userId: req.user.id
		}
		product.orders.push(orders)
		return product.save().then(product=>true).catch(err=>err.message)
	})
	if(isOrderUpdated !== true){
		return res.send({message: isOrderUpdated})
	}

	if(isUserUpdated&&isOrderUpdated){
		return res.send({message: "Thank you for Purchasing!"})
	}	
}

//get user details
module.exports.getUserDetails = (req,res)=>{
	console.log(req.user)
		User.findById(req.user.id)
		.then(result => res.send(result))
		.catch(error => res.send(error))
}
//make user admin (admin)
module.exports.makeAdmin = (req,res)=>{
	let update = {
		isAdmin: true
	}
	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//get user orders
module.exports.getOrders = (req,res)=>{
	console.log(req.user)
		User.findById(req.user.id)
		.then(result => res.send(result.orders))
		.catch(error => res.send(error))
}
	
//get all orders (admin only)
module.exports.getAllOrders = (req,res)=>{
		User.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
}





