const Product = require("../models/Product");
const auth = require("../auth");

//retrieve all active products
module.exports.getActiveProducts=(req,res)=>{
	Product.find({isActive:true})
	.then(result =>res.send(result))
	.catch(error => res.send(error))
}

//retieve single product
module.exports.getSingleProduct = (req,res)=>{
	console.log(req.params.orderId);
		Product.findById(req.params.orderId)
		.then(result => res.send(result))
		.catch(error => res.send(error))
}

//add product (admin)
module.exports.addProduct = (req,res)=>{
		let newProduct = new Product ({
			name : req.body.name,
			description: req.body.description,
			price: req.body.price
		})
		newProduct.save()
		.then(result => res.send(result))
		.catch(error => res.send(error))
	}

//update poduct information (admin)
module.exports.updateProduct = (req,res)=>{
	// console.log(req.params.orderId);
	// console.log(req.body);
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	Product.findByIdAndUpdate(req.params.orderId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//archive product (admin)
module.exports.archiveProduct = (req,res)=>{
	let update = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.orderId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//retrieve all orders(admin)
module.exports.getAllOrders = (req,res)=>{
		Product.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
}

//STRECHGOAL activate product (admin)
module.exports.activateProduct = (req,res)=>{
	let update = {
		isActive: true
	}
	Product.findByIdAndUpdate(req.params.orderId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//STRECHGOAL display products per order
module.exports.displayPerOrder = (req,res)=>{
 		Product.findById(req.params.orderId)
 		// if(product.orders = req.params.orderId){
 		// 	console.log("success");
 		// }else{
 		// 	console.log("wrong");
 		// }
		.then(result => res.send(result))
		.catch(error => res.send(error))
}
//for checking , show all products===========
// module.exports.getAllProducts = (req,res)=>{
// 		Product.find({})
// 		.then(result => res.send(result))
// 		.catch(error => res.send(error))
// }